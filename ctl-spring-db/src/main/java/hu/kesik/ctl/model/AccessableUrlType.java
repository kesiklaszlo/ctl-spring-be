package hu.kesik.ctl.model;

public enum AccessableUrlType {
	ACTIVATION,
	PASSWORD_RESET
}
