package hu.kesik.ctl.model;

public enum GoalHardness {
	BEGINNER,
	INTERMEDIATE,
	PROGRESSION
}
