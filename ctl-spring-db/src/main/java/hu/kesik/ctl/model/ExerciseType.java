package hu.kesik.ctl.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ExerciseTypes")
public class ExerciseType {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "exercisetype_generator")
	@SequenceGenerator(name="exercisetype_generator", sequenceName = "exercisetype_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private Integer id;

	@Column(nullable=false)
	private String name;

	@Column
	private String imageUrl;

	@OneToMany(targetEntity = Exercise.class, mappedBy = "exerciseType")
	private List<Exercise> exercises;

	@ManyToMany(targetEntity = MuscleType.class, mappedBy = "exerciseTypes")
	private List<MuscleType> muscleTypes;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public List<MuscleType> getMuscleTypes() {
		return muscleTypes;
	}

	public void setMuscleTypes(List<MuscleType> muscleTypes) {
		this.muscleTypes = muscleTypes;
	}

	public List<Exercise> getExercises() {
		return exercises;
	}

	public void setExercises(List<Exercise> exercises) {
		this.exercises = exercises;
	}

}
