package hu.kesik.ctl.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="MuscleTypes")
public class MuscleType {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "muscletype_generator")
	@SequenceGenerator(name="muscletype_generator", sequenceName = "muscletype_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private Integer id;

	@Column
	private String name;

	@Column
	private String imageUrl;

	@Column
	private String description;

	@ManyToMany(targetEntity= ExerciseType.class)
	@JoinTable(name="MuscleType_ExerciseTypes")
	private List<ExerciseType> exerciseTypes;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ExerciseType> getExerciseTypes() {
		return exerciseTypes;
	}

	public void setExerciseTypes(List<ExerciseType> exerciseTypes) {
		this.exerciseTypes = exerciseTypes;
	}

}
