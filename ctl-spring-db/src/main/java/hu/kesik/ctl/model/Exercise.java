package hu.kesik.ctl.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="Exercises")
public class Exercise {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "exercise_generator")
	@SequenceGenerator(name="exercise_generator", sequenceName = "exercise_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private Integer id;

	@Column(nullable = false)
	private String name;

	private String imageUrl;
 
	@Lob
	private String performance;

	@Lob
	private String exerciseXRay;

	@Lob
	private String perfectingTechnique;

	private Integer level;

	@ManyToOne(targetEntity = ExerciseType.class)
	private ExerciseType exerciseType;

	@OneToMany(targetEntity = Goal.class, mappedBy = "exercise")
	private List<Goal> goals;

	@OneToMany(targetEntity = Repetition.class, mappedBy = "exercise")
	private List<Repetition> repetitions;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public ExerciseType getExerciseType() {
		return exerciseType;
	}

	public void setExerciseType(ExerciseType exerciseType) {
		this.exerciseType = exerciseType;
	}

	public List<Goal> getGoals() {
		return goals;
	}

	public void setGoals(List<Goal> goals) {
		this.goals = goals;
	}

	public List<Repetition> getRepetitions() {
		return repetitions;
	}

	public void setRepetitions(List<Repetition> repetitions) {
		this.repetitions = repetitions;
	}

	public String getPerformance() {
		return performance;
	}

	public void setPerformance(String performance) {
		this.performance = performance;
	}

	public String getExerciseXRay() {
		return exerciseXRay;
	}

	public void setExerciseXRay(String exerciseXRay) {
		this.exerciseXRay = exerciseXRay;
	}

	public String getPerfectingTechnique() {
		return perfectingTechnique;
	}

	public void setPerfectingTechnique(String perfectingTechnique) {
		this.perfectingTechnique = perfectingTechnique;
	}

}
