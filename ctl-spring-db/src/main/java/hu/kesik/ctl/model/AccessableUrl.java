package hu.kesik.ctl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="AccessableUrls")
public class AccessableUrl {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "accessableurl_generator")
	@SequenceGenerator(name="accessableurl_generator", sequenceName = "accessableurl_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private Integer id;

	@ManyToOne(targetEntity = User.class)
	private User user;

	@Enumerated(EnumType.STRING)
	private AccessableUrlType type;

	private String gid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public AccessableUrlType getType() {
		return type;
	}

	public void setType(AccessableUrlType type) {
		this.type = type;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public String getGid() {
		return gid;
	}

}
