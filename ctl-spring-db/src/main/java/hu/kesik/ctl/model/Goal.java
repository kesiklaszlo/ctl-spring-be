package hu.kesik.ctl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="MuscleTypes")
public class Goal {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "goal_generator")
	@SequenceGenerator(name="goal_generator", sequenceName = "goal_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private Integer id;

	@Enumerated(EnumType.STRING)
	private GoalHardness hardness;

	private Integer sets;

	private Integer reps;

	private Integer seconds;

	@Enumerated(EnumType.STRING)
	private GoalType goalType;

	@ManyToOne(targetEntity = Exercise.class)
	private Exercise exercise;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public GoalHardness getHardness() {
		return hardness;
	}

	public void setHardness(GoalHardness hardness) {
		this.hardness = hardness;
	}

	public Integer getSets() {
		return sets;
	}

	public void setSets(Integer sets) {
		this.sets = sets;
	}

	public Integer getReps() {
		return reps;
	}

	public void setReps(Integer reps) {
		this.reps = reps;
	}

	public Integer getSeconds() {
		return seconds;
	}

	public void setSeconds(Integer seconds) {
		this.seconds = seconds;
	}

	public GoalType getGoalType() {
		return goalType;
	}

	public void setGoalType(GoalType goalType) {
		this.goalType = goalType;
	}

	public Exercise getExercise() {
		return exercise;
	}

	public void setExercise(Exercise exercise) {
		this.exercise = exercise;
	}

}
