package hu.kesik.ctl.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="Trainings")
public class Training {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "training_generator")
	@SequenceGenerator(name="training_generator", sequenceName = "training_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private Integer id;

	private LocalDateTime startTime;

	private LocalDateTime endTime;

	private String comment;

	@OneToMany(targetEntity = Repetition.class, mappedBy = "training")
	private List<Repetition> repetitions;

	@ManyToOne(targetEntity = User.class)
	private User user;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<Repetition> getRepetitions() {
		return repetitions;
	}

	public void setRepetitions(List<Repetition> repetitions) {
		this.repetitions = repetitions;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
