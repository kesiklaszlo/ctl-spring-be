package hu.kesik.ctl.repository;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

import hu.kesik.ctl.model.UserRole;

@Transactional
public interface UserRoleRepository extends JpaRepository<UserRole, Integer>{

}
