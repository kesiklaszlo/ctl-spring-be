package hu.kesik.ctl.repository;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

import hu.kesik.ctl.model.Exercise;

@Transactional
public interface ExerciseRepository extends JpaRepository<Exercise, Integer> {

}
