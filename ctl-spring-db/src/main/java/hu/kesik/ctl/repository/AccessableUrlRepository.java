package hu.kesik.ctl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import hu.kesik.ctl.model.AccessableUrl;

@Transactional
public interface AccessableUrlRepository extends JpaRepository<AccessableUrl, Integer>{

}
