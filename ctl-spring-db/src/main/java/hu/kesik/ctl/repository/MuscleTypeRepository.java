package hu.kesik.ctl.repository;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

import hu.kesik.ctl.model.MuscleType;

@Transactional
public interface MuscleTypeRepository extends JpaRepository<MuscleType, Integer>{

}
