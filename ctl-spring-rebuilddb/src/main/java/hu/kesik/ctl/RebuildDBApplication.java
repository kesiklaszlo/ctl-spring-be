package hu.kesik.ctl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class RebuildDBApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(RebuildDBApplication.class);
		app.setWebEnvironment(false);
		ConfigurableApplicationContext context = app.run(args);
	}
}
