package hu.kesik.ctl.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hu.kesik.ctl.dto.user.UserActivationDTO;
import hu.kesik.ctl.dto.user.UserActivationWrapper;
import hu.kesik.ctl.dto.user.UserEmailResponseMockView;
import hu.kesik.ctl.dto.user.UserInfoWrapper;
import hu.kesik.ctl.service.interfaces.UserService;
import hu.kesik.ctl.util.ControllerUtil;

@RequestMapping("/users")
@RestController
public class UserController {
	
	@Autowired
	private UserService userService;

	@PostMapping("/register")
	public ResponseEntity<?> register(@Valid @RequestBody UserInfoWrapper wrapper, BindingResult result) {
		try{
			ControllerUtil.validateInput(result);	
			
			UserActivationDTO dto = userService.register(wrapper);
			UserEmailResponseMockView view = new UserEmailResponseMockView();
			view.setEmail(dto.getEmail());
			view.setActivationGID(dto.getActivationGID());
						
			return ControllerUtil.ok(view);
		} catch(Exception e){
			return ControllerUtil.handleExceptions(e);
		}
	}
	
	@PostMapping("/activate")
	public ResponseEntity<?> activate(@Valid @RequestBody UserActivationWrapper wrapper, BindingResult result) {
		try{
			ControllerUtil.validateInput(result);
			userService.activate(wrapper);
			return ControllerUtil.ok();
		} catch(Exception e){
			return ControllerUtil.handleExceptions(e);
		}
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> activate(@PathVariable("id")Integer id) {
		try{
			userService.delete(id);
			return ControllerUtil.ok();
		} catch(Exception e){
			return ControllerUtil.handleExceptions(e);
		}
		
	}
}
