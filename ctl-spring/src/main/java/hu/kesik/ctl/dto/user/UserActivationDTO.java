package hu.kesik.ctl.dto.user;

public class UserActivationDTO {

	private String firstName;
	private String lastName;
	private String email;
	private String activationGID;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getActivationGID() {
		return activationGID;
	}

	public void setActivationGID(String activationGID) {
		this.activationGID = activationGID;
	}

}
