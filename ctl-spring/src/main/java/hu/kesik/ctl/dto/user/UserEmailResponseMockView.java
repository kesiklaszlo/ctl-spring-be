package hu.kesik.ctl.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import hu.kesik.ctl.dto.View;

public class UserEmailResponseMockView implements View {

	private String email;

	@JsonProperty("activationCode")
	private String activationGID;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getActivationGID() {
		return activationGID;
	}

	public void setActivationGID(String activationGID) {
		this.activationGID = activationGID;
	}

}
