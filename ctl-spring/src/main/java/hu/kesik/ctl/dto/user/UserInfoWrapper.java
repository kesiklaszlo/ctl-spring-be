package hu.kesik.ctl.dto.user;

import javax.validation.constraints.NotNull;

import hu.kesik.ctl.dto.Wrapper;

public class UserInfoWrapper implements Wrapper {

	@NotNull
	private String email;

	@NotNull
	private String password;

	private String firstName;

	private String lastName;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
