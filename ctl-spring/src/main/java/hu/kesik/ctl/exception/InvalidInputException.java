package hu.kesik.ctl.exception;

public class InvalidInputException extends Exception {
	private static final long serialVersionUID = -2209539536653444151L;

	public InvalidInputException() {
		super("Invalid input");
	}
	
	public InvalidInputException(String errorMsg) {
		super(errorMsg);
	}
}
