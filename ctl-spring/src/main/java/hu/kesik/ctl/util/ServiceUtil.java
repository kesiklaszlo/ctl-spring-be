package hu.kesik.ctl.util;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;

public class ServiceUtil {

	public static Exception getException(Exception e, Logger logger, String exceptionMessage) throws Exception {
		logger.error(exceptionMessage + ExceptionUtils.getStackTrace(e));
		return e;
	}
	
	public static Exception getException(Exception e, Logger logger) throws Exception {
		return getException(e, logger, e.getMessage());
	}
}
