package hu.kesik.ctl.util;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import hu.kesik.ctl.dto.View;
import hu.kesik.ctl.exception.InvalidInputException;

public class ControllerUtil {

	public static void validateInput(BindingResult result) throws InvalidInputException{
		if(result.hasErrors()){
			StringBuilder sb = new StringBuilder();
			sb.append(result.getGlobalErrors()
					.stream()
					.map(x -> x.getDefaultMessage())
					.collect(Collectors.joining("; ")));
			
			sb.append(result.getFieldErrors()
					.stream()
					.map(x -> "'"+x.getField() + "' " + x.getDefaultMessage())
					.collect(Collectors.joining("; ")));
			throw new InvalidInputException(sb.toString());
		}
	}
	
	public static ResponseEntity<?> handleExceptions(Exception e) {
		return handleExceptions(e, null);
	}
	
	public static ResponseEntity<?> handleExceptions(Exception e, String errorMessage){
		Map<String, Object> result = new HashMap<>();
		if(errorMessage == null) {
			errorMessage = e.getMessage();
		}
		if(e instanceof DataIntegrityViolationException) {
			result.put("errorMessage", "Constraint violation!");
			return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
		}
		
		result.put("errorMessage", errorMessage);
		return ResponseEntity.badRequest().body(result); 
	}
	
	public static ResponseEntity<?> ok(){
		Map<String, Object> result = new HashMap<>();
		result.put("message", "OK");
		return ResponseEntity.ok(result);
	}
	
	public static ResponseEntity<?> ok(View view){
		return ResponseEntity.ok(view);
	}
}
