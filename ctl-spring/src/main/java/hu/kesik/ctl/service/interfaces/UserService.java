package hu.kesik.ctl.service.interfaces;

import hu.kesik.ctl.dto.user.UserActivationDTO;
import hu.kesik.ctl.dto.user.UserActivationWrapper;
import hu.kesik.ctl.dto.user.UserInfoWrapper;

public interface UserService {

	public UserActivationDTO register(UserInfoWrapper userInfoWrapper) throws Exception;

	public void activate(UserActivationWrapper userActivationWrapper) throws Exception;

	public void delete(Integer userId) throws Exception;
}
