package hu.kesik.ctl.service.implementation;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import hu.kesik.ctl.dto.user.UserActivationDTO;
import hu.kesik.ctl.dto.user.UserActivationWrapper;
import hu.kesik.ctl.dto.user.UserInfoWrapper;
import hu.kesik.ctl.model.AccessableUrl;
import hu.kesik.ctl.model.AccessableUrlType;
import hu.kesik.ctl.model.User;
import hu.kesik.ctl.repository.AccessableUrlRepository;
import hu.kesik.ctl.repository.UserRepository;
import hu.kesik.ctl.service.interfaces.UserService;
import hu.kesik.ctl.util.ServiceUtil;

@Service
public class UserServiceImpl implements UserService {
	
	private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AccessableUrlRepository accessableUrlRepository;
	
	@Override
	public UserActivationDTO register(UserInfoWrapper userInfoWrapper) throws Exception {
		try{
			User user = new User();
			user.setEmail(userInfoWrapper.getEmail());
			user.setPassword(userInfoWrapper.getPassword());
			user.setFirstName(userInfoWrapper.getFirstName());
			user.setLastName(userInfoWrapper.getLastName());
			
			userRepository.save(user);
			
			AccessableUrl validationUrl = new AccessableUrl();
			validationUrl.setType(AccessableUrlType.ACTIVATION);
			validationUrl.setGid(UUID.randomUUID().toString());
			validationUrl.setUser(user);
			accessableUrlRepository.save(validationUrl);
			
			UserActivationDTO userActivationDTO = new UserActivationDTO();
			userActivationDTO.setEmail(user.getEmail());
			userActivationDTO.setFirstName(user.getFirstName());
			userActivationDTO.setLastName(user.getLastName());
			userActivationDTO.setActivationGID(validationUrl.getGid());
			return userActivationDTO;
		} catch(Exception e){
			throw ServiceUtil.getException(e, LOGGER);
		}
	}

	@Override
	public void activate(UserActivationWrapper userActivationWrapper) throws Exception {
		try {
			User user = new User();
			user.setEmail(userActivationWrapper.getEmail());

			Example<User> example = Example.of(user);
			user = userRepository.findOne(example).get();

			if (user != null) {
				AccessableUrl url = user.getAccessableUrls().stream().filter(
						a -> (a.getGid().equals(userActivationWrapper.getActivationGID()) 
								&& a.getType().equals(AccessableUrlType.ACTIVATION)))
						.findFirst().get();
				user.setActive(true);
				accessableUrlRepository.delete(url);
			}
		} catch (Exception e) {
			throw ServiceUtil.getException(e, LOGGER);
		}
	}

	@Override
	public void delete(Integer userId) throws Exception {
		try {
			userRepository.deleteById(userId);
		}catch (Exception e) {
			throw ServiceUtil.getException(e, LOGGER);
		}
	}

}
