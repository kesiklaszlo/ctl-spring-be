package hu.kesik.ctl.service.interfaces;

import hu.kesik.ctl.dto.user.UserActivationDTO;

public interface MailService {

	public void sendActivationMail(UserActivationDTO userActivationDTO) throws Exception;
	
}
