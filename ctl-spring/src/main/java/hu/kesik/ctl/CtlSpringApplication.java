package hu.kesik.ctl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CtlSpringApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(CtlSpringApplication.class, args);
	}
}
